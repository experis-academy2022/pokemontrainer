import { Pokemon } from "./pokemon.model";

export interface PokemonData {
    count: number;
    next: string;
    previous: null | string;
    results: Pokemon[];
}    