import { Component } from '@angular/core';
import { CatalogueService } from './services/catalogue.service';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'pokemonTrainer';

  constructor(
    private readonly userService: UserService,
    private readonly pokemonService: CatalogueService
  ) {}

  ngOnInit(): void {
    if (this.userService.user) {
      this.pokemonService.findAllPokemons()
    }
  }

}
