import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { CatchService } from 'src/app/services/catch.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-catch-button',
  templateUrl: './catch-button.component.html',
  styleUrls: ['./catch-button.component.scss']
})
export class CatchButtonComponent implements OnInit {

  public isCaught: boolean = false;
  @Input() pokemonName: string = ''; 

  public loading: boolean = false

  constructor(
    private userService: UserService,
    private readonly catchService: CatchService
  ) { }

  ngOnInit(): void {
    // Inputs are resolved!
    this.isCaught = this.userService.inCatched(this.pokemonName);
  }

  onCatchClick(): void {
    this.loading = true
    // add the pokemon to catched
    this.catchService.addToCatched(this.pokemonName)
    .subscribe({
      next: (response: User) => {
        this.loading = false
        this.isCaught = this.userService.inCatched(this.pokemonName)
      },
      error: (error: HttpErrorResponse) => {
        console.log("ERROR", error.message)
      }
    })
  }
}
