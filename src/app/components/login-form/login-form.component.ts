import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
// import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { LoginService } from 'src/app/services/login.service';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  @Output() login: EventEmitter<void> = new EventEmitter()

  // DI
  constructor(
    // private readonly router: Router,
    private readonly loginService: LoginService,
    private readonly userService: UserService
    ) { }

  public loginSubmit(loginForm: NgForm): void {

    // username!
    const {username} = loginForm.value

    this.loginService.login(username)
    .subscribe({
      next: (user: User) => {
        // redirect to catalogue page
        // this.router.navigateByUrl("/catalogue")
        this.login.emit()
        this.userService.user = user
      },
      error: () => {

      }

    })
  }

  ngOnInit(): void {
  }

}
