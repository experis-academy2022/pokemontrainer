import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { StorageKeys } from '../enums/storage-keys.enum';
import { User } from '../models/user.model';
import { StorageUtil } from '../utils/storage.util';

const { apiUsers, apiKey } = environment

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  // Dependency injection
  constructor (private readonly http: HttpClient) { }

  // Models, HttpClient, Observables, and RxJS operators
  public login(username: string): Observable <User> {
    return this.checkUsername(username)
      .pipe(
        switchMap((response: User | undefined) => {
          if (response === undefined) { // user does not exist
            return this.createUser(username)
          }
          return of (response)
        }),
        tap((user: User) => {
          StorageUtil.storageSave<User>(StorageKeys.User, user)
        })
      )
  }

  // Login

  // Check if user exists
  private checkUsername (username: String): Observable <User|undefined> {
    return this.http.get<User[]>(`${apiUsers}?username=${username}`)
      .pipe (
        // RxJS Operators
        map((response: User[]) => {return response.pop()})
      )
  }

  // IF NOT user - Create a User
  private createUser(username: string): Observable<User> {

    // user
    const user = {
      username,
      pokemons: []
    }

    // headers -> API key
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": apiKey
    })

    // POST - Create items on the server
    return this.http.post<User>(apiUsers, user, {headers})

  }


  // IF User || Created User -> Store user
}
