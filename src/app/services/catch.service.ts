import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable, tap } from 'rxjs';
import { Pokemon } from '../models/pokemon.model';
import { User } from '../models/user.model';
import { CatalogueService } from './catalogue.service';
import { UserService } from './user.service';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class CatchService {

  /*private _loading: boolean = false

  get loading(): boolean {
    return this._loading
  } */

  constructor(
    private http: HttpClient,
    private readonly pokemonService: CatalogueService,
    private readonly userService: UserService
  ) { }

  // get the pokemon based on name
  // patch request with the userID and the pokemon

  public addToCatched(pokemonName: string): Observable<User> {

    if (!this.userService.user) {
      throw new Error("addToCatched: There is no trainer")
    }

    const user: User = this.userService.user
    // here is some problem with pokemon data and pokemon names
    const pokemon: Pokemon | undefined =  this.pokemonService.pokemons.find(p => p.name === pokemonName)

    if (!pokemon) {
      throw new Error("addToCatched: No pokemon with name: " + pokemonName)
    }

    if (this.userService.inCatched(pokemonName)) {
      // throw new Error("addToCatched: Pokemon already catched.")
      this.userService.removeFromCatched(pokemonName)
    } else {
      this.userService.addToCatched(pokemon)
    }

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': environment.apiKey
    })

    // this._loading = true

    return this.http
    .patch<User>(`${environment.apiUsers}/${user.id}`, {
      pokemons: [...user.pokemons] // Already updated
    }, {
      headers
    })
    .pipe(
      tap((updatedUser: User) => {
        this.userService.user = updatedUser
      })/*,
      finalize(() => {this._loading = false})*/
    )

  }

}
