# Pokemon Trainer

Pokemon trainer is a web app in which you can catch pokemons from a list and see your list of caught ones. It was created as an exercise at Experis Academy to learn Angular and TypeScript.

You can use the app here: https://mm-pokemon-trainer.herokuapp.com/

## Features
The user is able to:

* Create an account by determining a username
* Login/Logout
* View a list of pokemons after logging in
* Catch pokemons from the list
* View caught pokemons
* Remove caught pokemons 

## Installation
* To run the code on local machine you will need access to the API (environments file) or create your own backend.
* You will also need Node.js: https://nodejs.org/en/download/ and git: https://git-scm.com/downloads
* In your console navigate to the folder where you want to install the project and run the following commands:

```bash
git clone https://gitlab.com/experis-academy2022/pokemontrainer.git
```
```bash
cd pokemontrainer
```
```bash
npm install 
```

* For API access, create an environments folder with "environments.ts" file in the src folder of the project.
* Add the following lines and paste the correct API key and url inside the quotation marks:
```bash
export const environment = {
  production: false,
  apiUsers: "",
  apiKey: ""
};
```

## Usage
- You're able to run the App in the development mode by typing "ng serve" in the command line after installation. Open http://localhost:4200 to view it in the browser.
- Enter a username to first create an account. Username must be at least 3 characters long.
- After logging in you will be redirected to a catalogue page where one can view and catch pokemons. 
- From the top right corner you can go to the trainers page where you can see the caught pokemons and remove them from your list.

## Contributors
- Maxim Anikin      https://gitlab.com/MaximusAnakin
- Marie Puhakka     https://gitlab.com/mariesusan

## License
[MIT](https://choosealicense.com/licenses/mit/)
